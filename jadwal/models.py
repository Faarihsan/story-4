from django.db import models

class Jadwal(models.Model):
    title = models.CharField(max_length=100)
    date = models.DateField()
    waktu = models.TimeField()
    tempat = models.TextField()
    kategori = models.TextField()
