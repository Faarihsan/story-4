from django.shortcuts import render, redirect
from .models import Jadwal
from . import forms
from .forms import CreateJadwal

def jadwal(request):
    form = forms.CreateJadwal(request.POST, request.FILES)
    if request.method == "POST":
        if form.is_valid():
            ja = Jadwal(
                title=form.cleaned_data['title'],
                tempat = form.cleaned_data['tempat'],
                date = form.cleaned_data['date'],
                waktu = form.cleaned_data['waktu'],
                kategori = form.cleaned_data['kategori'],
                )
            ja.save()
            return redirect('jadwal:buat_jadwal')
        else :
            form = form.createJadwal() 
    jadwal = Jadwal.objects.all().order_by('date')
    return render(request, 'jadwal/jadwal.html', {'jadwal':jadwal, 'form': form})

def delete(request, id):
    Jadwal.objects.get(pk=id).delete()
    return redirect('jadwal:buat_jadwal')
