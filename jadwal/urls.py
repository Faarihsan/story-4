from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'jadwal'

urlpatterns = [
    path('',views.jadwal, name='buat_jadwal'),
    path('delete/<int:id>/', views.delete, name='delete'),
]

urlpatterns += staticfiles_urlpatterns()