from . import models
from django import forms

class CreateJadwal(forms.Form):
    title = forms.CharField(label='title',  widget=forms.TextInput(attrs={
        'class' : "form-control",
        'type' : "text",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }), max_length=50)

    tempat = forms.CharField(label='tempat', required=False, widget=forms.TextInput(attrs={
        'class' : "form-control",
        'type' : "text",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))
    kategori = forms.CharField(label='kategori', required=False, widget=forms.TextInput(attrs={
        'class' : "form-control",
        'type' : "text",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))
    date = forms.DateField(label="date", widget=forms.DateInput(attrs={
        'class' : "form-control",
        'type' : "date",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))
    
    waktu = forms.TimeField(label="waktu", required=True, widget=forms.TimeInput(attrs={
        'class' : "form-control",
        'type' : "time",
        'aria-label' : "Default",
        'aria-describedby' : "inputGroup-sizing-default",
        }))

    
    class Meta:
        model = models.Jadwal
        fields = ['title', 'detail', 'date', 'waktu']