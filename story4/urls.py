from django.contrib import admin
from django.urls import path, include
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.homepage),
    path('experience',views.experience),
    path('education',views.education),
    path('project',views.project),
    path('gallery',views.gallery),
    path('jadwal/', include('jadwal.urls')),
]

urlpatterns += staticfiles_urlpatterns()