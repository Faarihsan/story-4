from django.http import HttpResponse
from django.shortcuts import render

def homepage(request):
    return render(request, 'index.html')

def education(request):
    return render(request, 'education.html')

def experience(request):
    return render(request, 'experience.html')

def project(request):
    return render(request, 'project.html')

def gallery(request):
    return render(request, 'gallery.html')